from filter_eval.confmat import BinaryConfusionMatrix
from common.utils import read_classification_from_file
from common.constants import *

def quality_score(tp, tn, fp, fn):
    true_eval = tp + tn
    false_eval = 10 * fp + fn
    return true_eval / (true_eval + false_eval)


def get_conf_matrix(corpus_dir):
    truth_dict = read_classification_from_file(
        '{}/{}'.format(corpus_dir, TRUTH_FILE))
    pred_dict = read_classification_from_file(
        '{}/{}'.format(corpus_dir, PREDICTION_FILE))

    conf_matrix = BinaryConfusionMatrix(SPAM_TAG, HAM_TAG)
    conf_matrix.compute_from_dicts(truth_dict, pred_dict)

    return conf_matrix
