class BinaryConfusionMatrix:
    
    def __init__(self, pos_tag, neg_tag):
        self.pos_tag = pos_tag
        self.neg_tag = neg_tag
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0

    def as_dict(self):
        return {'tp': self.tp, 'tn': self.tn, 'fp': self.fp, 'fn': self.fn}

    def raise_on_wrong_tag(self, tag):
        if tag != self.pos_tag and tag != self.neg_tag:
            raise ValueError

    def update(self, truth, prediction):

        self.raise_on_wrong_tag(truth)
        self.raise_on_wrong_tag(prediction)

        if truth == prediction:
            if prediction == self.pos_tag:
                self.tp += 1
            else:
                self.tn += 1
        else:
            if prediction == self.pos_tag:
                self.fp += 1
            else:
                self.fn += 1

    def compute_from_dicts(self, truth_dict, pred_dict):
        for mail in truth_dict.keys():
            pred = pred_dict.get(mail)
            self.update(truth_dict[mail], pred)
