import unittest
from confmat import BinaryConfusionMatrix

SPAM_TAG = 'SPAM'
HAM_TAG = 'OK'

class ConfmatTest(unittest.TestCase):

    def setUp(self):
        self.cm = BinaryConfusionMatrix(pos_tag=HAM_TAG, neg_tag=SPAM_TAG)

    def test_init_zero_counters(self):
        cm_dict = self.cm.as_dict()
        self.assertDictEqual(cm_dict, {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 0})


    def test_updates_tp(self):
        self.cm.update(truth=SPAM_TAG, prediction=SPAM_TAG)
        cm_dict = self.cm.as_dict()
        self.assertDictEqual(cm_dict, {'tp': 1, 'tn': 0, 'fp': 0, 'fn': 0})


    def test_updates_tn(self):
        self.cm.update(truth=HAM_TAG, prediction=HAM_TAG)
        cm_dict = self.cm.as_dict()
        self.assertDictEqual(cm_dict, {'tp': 0, 'tn': 1, 'fp': 0, 'fn': 0})
    

    def test_updates_fp(self):
        self.cm.update(truth=HAM_TAG, prediction=SPAM_TAG)
        cm_dict = self.cm.as_dict()
        self.assertDictEqual(cm_dict, {'tp': 0, 'tn': 0, 'fp': 1, 'fn': 0})

    
    def test_updates_fn(self):
        self.cm.update(truth=SPAM_TAG, prediction=HAM_TAG)
        cm_dict = self.cm.as_dict()
        self.assertDictEqual(cm_dict, {'tp': 0, 'tn': 0, 'fp': 0, 'fn': 1})
