import filter_eval.quality as quality
import os
from filter import MyFilter

my_filter = MyFilter()
my_filter.train('')
my_filter.train('data/2/')
my_filter.test('data/2/')
conf_matrix = quality.get_conf_matrix('data/2/').as_dict()
print(conf_matrix)
filter_quality = quality.quality_score(**conf_matrix)
print('Quality: {}%'.format(int(filter_quality * 100)))
os.remove('data/2/!prediction.txt')
