from common.mail_parser import MailParser
from classifiers.learning_data import LearningData


class WordsClassifier:
    '''Defines probability of each word beeing contained in spam'''

    def __init__(self, learning_data=None):
        if not learning_data:
            learning_data = LearningData({})
        self.learning_data = learning_data

    def get_prob(self, word, mails_subset):
        total = len(mails_subset)
        containing_word = len(list(
            filter(lambda m: word in m.body, mails_subset)))
        return containing_word / total

    def get_mails_data(self, mails):
        words = set()
        spams, hams = [], []

        parser = MailParser()
        for mail in map(lambda m: parser.parse_mail(m), mails):
            words |= mail.body
            if mail.is_spam:
                spams.append(mail)
            else:
                hams.append(mail)

        return words, spams, hams

    def update_words_weights(self, mails):

        words, spams, hams = self.get_mails_data(mails)

        sp_prior_prob = len(spams)/len(mails) if mails\
            else self.learning_data.spam_prob

        self.learning_data.spam_prob = sp_prior_prob

        for word in words:
            if self.learning_data.words_weights.__contains__(word):
                sp_prior_prob = self.learning_data.words_weights[word]
                if sp_prior_prob == 1:
                    sp_prior_prob -= 1/1000
                if sp_prior_prob == 0:
                    sp_prior_prob += 1/1000

            w_s = sp_prior_prob * self.get_prob(word, spams)
            w_h = (1 - sp_prior_prob) * self.get_prob(word, hams)
            weight = w_s / (w_s + w_h)
            self.learning_data.words_weights[word] = weight
