import json
from classifiers.params import SPAM_DEF_PRIOR_PROB


class LearningData:

    save_path = 'learning_data.json'

    def __init__(self, words_weights):
        self.words_weights = words_weights
        self.spam_prob = SPAM_DEF_PRIOR_PROB

    def save(self):
        with open(self.save_path, 'w', encoding='utf-8') as fp:
            json.dump(self.words_weights, fp)

    @staticmethod
    def restore():
        try:
            with open(LearningData.save_path, 'r', encoding='utf-8') as fp:
                return LearningData(json.load(fp))
        except FileNotFoundError:
            return None
