SPAM_MIN_PROB = 0.8  # level of spamity when identified as a spam
# prior probability of mail beeing spam, not depending on dataset
SPAM_DEF_PRIOR_PROB = 0.8
HAM_ZERO_PROB = 1/300  # if word not found in dataset ham
SPAM_ZERO_PROB = 1/500
