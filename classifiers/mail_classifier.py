from common.mail_parser import MailParser
from classifiers.learning_data import LearningData
from classifiers.params import SPAM_MIN_PROB


class MailClassifier:
    '''Combines words spam probabilities to mail spam probability'''

    def __init__(self, learning_data):
        self.learning_data = learning_data
        self.words_spamity = 1
        self.words_hamity = 1

    def update_words_eval(self, word_weight, bias):
        self.words_spamity *= word_weight if word_weight != 0.0 else 1/500
        word_hamity = 1 - word_weight
        self.words_hamity *= word_hamity * bias if word_hamity != 0.0 else 1/300

    def evaluate_words(self, mail_words):
        bias_pow = 0
        for word in mail_words:
            word_weight = self.learning_data.words_weights.get(word)
            bias_pow += 1
            if word_weight:
                prior_spam_prob = self.learning_data.spam_prob
                bias = (prior_spam_prob / (1 - prior_spam_prob))\
                 if bias_pow != len(mail_words) else 1
                self.update_words_eval(word_weight, bias)

    def is_spam(self, mail):

        parsed_mail = MailParser().parse_mail(mail)

        if parsed_mail.is_spam != None:
            return parsed_mail.is_spam

        self.words_hamity = self.words_spamity = 1
        
        self.evaluate_words(parsed_mail.body)

        if self.words_hamity != 0 or self.words_spamity != 0:
            spam_prob = self.words_spamity / \
                (self.words_spamity + self.words_hamity)
        else:
            spam_prob = 0

        return spam_prob >= SPAM_MIN_PROB
