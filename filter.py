from basefilter import BaseFilter
from classifiers.words_classifier import WordsClassifier
from classifiers.mail_classifier import MailClassifier
from classifiers.learning_data import LearningData
from common.mail_data import MailData
import json


class MyFilter(BaseFilter):
    '''Biased naive Bayesian filter.'''

    def __init__(self):
        self.words_classifier = WordsClassifier()
        self.data_file = 'train_data.json'


    def train(self, corpus_dir):
        super().train(corpus_dir)
        mails = list(self.train_corpus.emails())

        pretrained = LearningData.restore()
        if pretrained:
            self.words_classifier.learning_data = pretrained

        self.words_classifier.update_words_weights(mails)

        self.words_classifier.learning_data.save()

    def test(self, corpus_dir):
        super().test(corpus_dir)

        learning_data = self.words_classifier.learning_data
        mail_classifier = MailClassifier(learning_data)

        evald_mails = []
        for mail in self.corpus.emails():
            is_spam = mail_classifier.is_spam(mail)
            evald_mails.append(MailData(mail.name, None, is_spam))

        super().write_prediction(evald_mails)
