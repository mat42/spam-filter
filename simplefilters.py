from basefilter import BaseFilter
from common.mail_data import MailData


class NaiveFilter(BaseFilter):

    def test(self, corpus_dir):
        super().test(corpus_dir)

        evald_mails = []
        for mail in self.corpus.emails():
            evald_mails.append(MailData(mail.name, mail.body, False))

        super().write_prediction(evald_mails)


class ParanoidFilter(BaseFilter):
    pass

class RandomFilter(BaseFilter):
    pass
