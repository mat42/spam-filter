from data_corpuses.corpus import Corpus
from common.utils import read_classification_from_file
from common.mail_data import MailData
from common.constants import TRUTH_FILE, SPAM_TAG


class TrainCorpus(Corpus):

    def __init__(self, dir_path):
        super().__init__(dir_path)

    def emails(self):
        truth_dict = read_classification_from_file(
            '{}/{}'.format(self.dir_path, TRUTH_FILE))
        for mail in super().emails():
            is_spam = truth_dict[mail.name] == SPAM_TAG
            yield MailData(mail.name, mail.body, is_spam)
