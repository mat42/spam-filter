import os
from common.mail_data import MailData


class Corpus:

    def __init__(self, dir_path):
        self.dir_path = dir_path

    def emails(self):

        try:
            for mail_path in os.listdir(self.dir_path):

                if mail_path[0] == '!':
                    continue

                with open('{}/{}'.format(self.dir_path, mail_path), 'r', encoding='utf-8') as mail_file:
                    yield MailData(mail_path, mail_file.read())
        except FileNotFoundError:
            return []
