from common.constants import SPAM_TAG, HAM_TAG, PREDICTION_FILE
from data_corpuses.corpus import Corpus
from data_corpuses.train_corpus import TrainCorpus


class BaseFilter:

    def __init__(self):
        self.corpus = None
        self.train_corpus = None

    def train(self, corpus_dir):
        self.train_corpus = TrainCorpus(corpus_dir)

    def test(self, corpus_dir):
        self.corpus = Corpus(corpus_dir)

    def write_prediction(self, evald_mails):
        path = '{}/{}'.format(self.corpus.dir_path, PREDICTION_FILE)
        with open(path, 'w', encoding='utf-8') as fo:
            for mail in evald_mails:
                mail_tag = SPAM_TAG if mail.is_spam else HAM_TAG
                fo.write('{} {}\n'.format(mail.name, mail_tag))
