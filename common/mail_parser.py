import re
import email


class MailParser:

    def __init__(self):
        self.re_patterns = ['[A-Z]{5,}', '\!+',
                            '(_|\W){6,}', '<[ab][^>]*>([^>]+)</[ab]>']
        self.msg = None
        self.msg_content_type = None

    def remove_garbege(self):

        if self.msg_content_type == 'text/html':
            self.mail_txt = re.sub('<.+>', '', self.mail_txt)

        self.mail_txt = re.sub('[0-9]+', '', self.mail_txt)

    def get_patterns(self):
        return set(filter(lambda p: re.match(p, self.mail_txt), self.re_patterns))

    def get_words(self):
        return set(filter(lambda w: w != str(),
                          map(lambda w: w.lower(), re.split('_|\W', self.mail_txt))))

    def parse_mail(self, mail):

        msg = email.message_from_string(mail.body)

        spam_status = dict(msg._headers).get('X-Spam-Status')
        if spam_status:
            mail.is_spam = spam_status == 'Yes'

        words = set()

        for part in msg.walk():

            self.msg_content_type = part.get_content_type()

            if self.msg_content_type != 'text/html'\
                    and self.msg_content_type != 'text/plain':
                continue

            self.mail_txt = part.get_payload()

            words |= self.get_patterns()

            self.remove_garbege()

            words |= self.get_words()

        mail.body = words
        return mail
