
def read_file(path):
    res_dict = {}
    with open(path, 'r', encoding='utf-8') as file:
        for line in file.readlines():
            mail_path, value = line.split()
            existing_val = res_dict.get(mail_path)
            if not existing_val:
                res_dict[mail_path] = value
    return res_dict


def read_classification_from_file(path):
    try:
        return read_file(path)
    except FileNotFoundError:
        pass
